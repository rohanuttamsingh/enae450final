# ENAE450 Final

## Instructions to Run This Code

In a shell:

```
source devel/setup.bash
roslaunch maze_solver [launchfile]
```

where `launchfile` is the desired launch file. To launch one of the provided mazes, use `maze0.launch`, `maze1.launch`, or `maze2.launch`.

In a separate shell:

```
source devel/setup.bash
rosrun maze_solver follow_wall.py
```

The robot should now begin solving the maze.