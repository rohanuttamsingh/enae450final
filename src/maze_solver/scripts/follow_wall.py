#! /usr/bin/env python

# import ros stuff
from math import sqrt
import time
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist


speed = 0.5
min_speed = 0.1
arc_range_front = (-5, 5)
arc_range_right = (268, 275)
arc_range_left = (88, 93)
forward = min_speed
turning = 0

def f(x):
    if x < 0.3:
        return (sqrt(x) - sqrt(0.4)) / 3
    return min((x ** 2 - 0.2 ** 2) / 3, 0.3)

def cb(msg):
    """
    Callback for setting the command velocities.
    """
    global turning, forward
    front, right, left = get_dirs(msg)
    print(right)

    if front < 0.35:
        forward = 0.02
        turning = 0.5
        return

    turning = -f(right) * 3
    forward = 0.25
           

def get_dirs(msg):
    """
    Uses the front and right ranges to grab the average values
    within the range.
    """
    # Front ranges from negative to positive, so logic slightly different
    front = sum(
        msg.ranges[arc_range_front[0]:] + msg.ranges[:arc_range_front[1]]
    ) / abs(arc_range_front[1] - arc_range_front[0])
    right = sum(
        msg.ranges[arc_range_right[0]:arc_range_right[1]]
    ) / abs(arc_range_right[1] - arc_range_right[0])
    left = sum(
        msg.ranges[arc_range_left[0]:arc_range_left[1]]
    ) / abs(arc_range_left[1] - arc_range_left[0])
    return front, right, left

def main():
    rospy.init_node('reading_laser')
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
    sub = rospy.Subscriber('/scan', LaserScan, cb)
    
    rate = rospy.Rate(20)
    while not rospy.is_shutdown():
        msg = Twist()
        msg.linear.x = forward
        msg.angular.z = turning
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    main()
